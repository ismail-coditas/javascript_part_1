// nested destructuring 
const users = [
    {userId: 1,firstName: 'faizan', gender: 'male'},
    {userId: 2,firstName: 'abishek', gender: 'male'},
    {userId: 3,firstName: 'dileep', gender: 'male'},
]
const [{firstName: user1firstName, userId}, , {gender: user3gender}] = users;
console.log(user1firstName);
console.log(userId);
console.log(user3gender);





