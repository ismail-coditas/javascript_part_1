// if-else condition 
let age = 15;
if(age>=18){
    console.log("User can play gta");
}else {
    console.log("User can play mario");
}

let num = 13;
if(num%2===0){
    console.log("even");
}else{
    console.log("odd");
}

// these are falsy values 
""
0
null 
undefined
false

// truthy 
"abc"
1, -1

// let firstName= 0;    0 empty 1 and -1 will be printed

// if(firstName){
//     console.log(firstName);
// }else{
//     console.log("firstName is kinda empty");
// }




















