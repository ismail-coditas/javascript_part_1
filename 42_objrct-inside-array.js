// objects inside array 
// very useful in real world applications
const users = [
    {userId: 1,firstName: 'faizan', gender: 'male'},
    {userId: 2,firstName: 'abishek', gender: 'male'},
    {userId: 3,firstName: 'dileep', gender: 'male'},
]
for(let user of users){
    console.log(user.firstName);
}




